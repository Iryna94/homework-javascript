
/*Разработать функцию-конструктор, которая будет создавать объект Human
создайте массив объектов и реализуйте функцию, которая будет сортировать 
элементы массива по значению свойства Age по возрастанию или по убыванию.
 */
function Human(name, age, nationality) {
    this.name = name;
    this.age = age;
    this.nationality = nationality;
}

var users = [];
users.push(new Human('Camilla', 20, 'French'), new Human('Helen', 35, 'Italian'), new Human('Lily', 48, 'Irish'));

console.log(users);



function sortbyAge(a, b) {
    return a.age > b.age ? 1 : a.age < b.age ? -1 : 0;
}
users.sort(sortbyAge)


users.forEach((element) => console.log(element));